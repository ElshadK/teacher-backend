﻿using System.Collections.Generic;
using System.Threading.Tasks;
using teacher_backend.DTOs;

namespace teacher_backend.Services.IServices
{
    public interface IPostService
    {
        Task<PostToListDTO> AddPost(PostAddDTO postDTO, int userId);
        Task<PostUpdateDTO> GetPost(int userId, int postId);
        Task DeletePost(int userId, int postId);
        Task UpdatePost(PostUpdateDTO postUpdateDTO, int userId);
        Task<List<PostToListDTO>> GetPosts(FilterPostDTO filter, PaginationDTO pagination);
        Task<List<PostToListDTO>> GetPostsForUser(int userId, PaginationDTO pagination);
    }
}


