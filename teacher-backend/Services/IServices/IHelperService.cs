﻿using System.Collections.Generic;
using System.Threading.Tasks;
using teacher_backend.DTOs;

namespace teacher_backend.Services.IServices
{
    public interface IHelperService
    {
        Task<List<SelectListDTO>> GetSubjects();
        Task<List<SelectListDTO>> GetRegions();
        Task<List<SelectListDTO>> GetSchools(int regionId);

    }
}
