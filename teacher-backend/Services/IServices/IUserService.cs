﻿using System.Threading.Tasks;
using teacher_backend.DTOs;

namespace teacher_backend.Services.IServices
{
    public interface IUserService
    {
        Task<UserToListDTO> GetUser(int userId);
        Task<UserToListDTO> AddUser(UserAddDTO userAddDTO);
        Task<UserToListDTO> UpdateUser(UserUpdateDTO userUpdateDTO, int userId);
        Task<UserToListDTO> LoginUser(UserLoginDTO userLoginDTO);
        Task ResetPassword(string mail, string password);
        Task<bool> IsMailExist(string mail);
        Task<bool> IsUserExist(string mail, string phone);
        Task<string> GetUserSalt(string mail);
        Task<string> GetUserSaltById(int userId);
        Task ChangePassword(ChangePasswordDTO changePasswordDTO, int userId);

    }
}
