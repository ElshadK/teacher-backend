﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using teacher_backend.DAL;
using teacher_backend.DTOs;
using teacher_backend.Entities;
using teacher_backend.Services.IServices;

namespace teacher_backend.Services
{
    public class PostService : IPostService
    {
        private readonly IMapper _mapper;
        private readonly DataContext _dataContext;
        public PostService(DataContext dataContext, IMapper mapper)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }

        public async Task<PostToListDTO> AddPost(PostAddDTO postDTO, int userId)
        {
            Post post = _mapper.Map<Post>(postDTO);
            post.CreatedAt = DateTime.Now;
            post.UserId = userId;
            //post.IsDeleted = false;
            post.IsShowMail = true;
            post.IsShowPhone = true;
            var added = await _dataContext.Posts.AddAsync(post);
            await _dataContext.SaveChangesAsync();
            return _mapper.Map<PostToListDTO>(added.Entity);
        }

        public async Task DeletePost(int userId, int postId)
        {
            Post post = await _dataContext.Posts.FirstOrDefaultAsync(x => x.UserId == userId && x.PostId == postId);
            if (post != null)
            {
                post.IsDeleted = true;
                await _dataContext.SaveChangesAsync();
            }

        }

        public async Task<PostUpdateDTO> GetPost(int userId, int postId)
        {
            return _mapper.Map<PostUpdateDTO>(await _dataContext.Posts.FirstOrDefaultAsync(x => x.UserId == userId && x.PostId == postId));
        }

        public async Task<List<PostToListDTO>> GetPosts(FilterPostDTO filter, PaginationDTO pagination)
        {
            List<Post> posts = await _dataContext.Posts.Where(x =>
                                       (filter.UserId != null ? filter.UserId == x.UserId : true) &&
                                       (filter.FromSchoolId != null ? filter.FromSchoolId == x.FromSchoolId : true) &&
                                       (filter.FromRegionId != null ? filter.FromRegionId == x.FromRegionId : true) &&
                                       (filter.ToSchoolId != null ? filter.ToSchoolId == x.ToSchoolId : true) &&
                                       (filter.ToRegionId != null ? filter.ToRegionId == x.ToRegionId : true) &&
                                       (filter.SubjectId != null ? filter.SubjectId == x.SubjectId : true) &&
                                       (filter.LessonTimeMin != null ? filter.LessonTimeMin <= x.LessonTime : true) &&
                                       (filter.LessonTimeMax != null ? filter.LessonTimeMax >= x.LessonTime : true))
                                   .OrderByDescending(x => x.CreatedAt).Skip((pagination.PageNumber - 1) * pagination.PageSize).Take(pagination.PageSize).ToListAsync();

            return _mapper.Map<List<PostToListDTO>>(posts);
        }

        public async Task<List<PostToListDTO>> GetPostsForUser(int userId, PaginationDTO pagination)
        {
            List<Post> posts = await _dataContext.Posts.Where(x => x.UserId == userId)
                                   .OrderByDescending(x => x.CreatedAt).Skip((pagination.PageNumber - 1) * pagination.PageSize).Take(pagination.PageSize).ToListAsync();

            return _mapper.Map<List<PostToListDTO>>(posts);
        }

        public async Task UpdatePost(PostUpdateDTO postDTO, int userId)
        {
            Post post = await _dataContext.Posts.FirstOrDefaultAsync(x => x.UserId == userId && x.PostId == postDTO.PostId);
            if (post != null)
            {
                post.FromRegionId = postDTO.FromRegionId;
                post.ToRegionId = postDTO.ToRegionId;
                post.FromSchoolId = postDTO.FromSchoolId;
                post.ToSchoolId = postDTO.ToSchoolId;
                post.LessonTime = postDTO.LessonTime;
                post.SubjectId = postDTO.SubjectId;
                post.Note = postDTO.Note;

                await _dataContext.SaveChangesAsync();
            }
        }
    }
}
