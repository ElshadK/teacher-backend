﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;
using teacher_backend.Core.Utility;
using teacher_backend.DAL;
using teacher_backend.DTOs;
using teacher_backend.Entities;
using teacher_backend.Services.IServices;

namespace teacher_backend.Services
{
    public class UserService : IUserService
    {
        private readonly IMapper _mapper;
        private readonly DataContext _dataContext;
        public UserService(DataContext dataContext, IMapper mapper)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }

        public async Task<UserToListDTO> AddUser(UserAddDTO userAddDTO)
        {
            User user = _mapper.Map<User>(userAddDTO);
            user.CreatedAt = DateTime.Now;
            user.Salt = SecurityHelper.GenerateSalt();
            user.Password = SecurityHelper.HashPassword(user.Password, user.Salt);
            var added = await _dataContext.Users.AddAsync(user);
            await _dataContext.SaveChangesAsync();
            return _mapper.Map<UserToListDTO>(added.Entity);
        }

        public async Task ChangePassword(ChangePasswordDTO changePasswordDTO, int userId)
        {
            User user = await _dataContext.Users.FirstOrDefaultAsync(m => m.UserId == userId);
            if (user != null)
            {
                user.Password = changePasswordDTO.NewPassword;
                await _dataContext.SaveChangesAsync();
            }
        }

        public async Task<UserToListDTO> GetUser(int userId)
        {
            return _mapper.Map<UserToListDTO>(await _dataContext.Users.FirstOrDefaultAsync(m => m.UserId == userId));
        }

        public async Task<string> GetUserSalt(string mail)
        {
            User user = await _dataContext.Users.FirstOrDefaultAsync(m => m.Mail.ToLower() == mail.ToLower());
            return user != null ? user.Salt : null;
        }

        public async Task<string> GetUserSaltById(int userId)
        {
            User user = await _dataContext.Users.FirstOrDefaultAsync(m => m.UserId == userId);
            return user != null ? user.Salt : null;
        }

        public async Task<bool> IsMailExist(string mail)
        {
            return await _dataContext.Users.AnyAsync(m => m.Mail.ToLower() == mail.ToLower());
        }

        public async Task<bool> IsUserExist(string mail, string phone)
        {
            return await _dataContext.Users.AnyAsync(m => m.Mail.ToLower() == mail.ToLower() || m.Phone == phone);
        }

        public async Task<UserToListDTO> LoginUser(UserLoginDTO userLoginDTO)
        {
            User user = await _dataContext.Users.FirstOrDefaultAsync(m => m.Mail.ToLower() == userLoginDTO.Mail.ToLower() && m.Password == userLoginDTO.Password);
            return _mapper.Map<UserToListDTO>(user);
        }

        public async Task ResetPassword(string mail, string password)
        {
            //string salt = SecurityHelper.GenerateSalt();
            //string userPassword = SecurityHelper.HashPassword(password, salt);
            //await _unitOfWork.UserRepository.ResetPassword(email, salt, userPassword);
            //await _unitOfWork.Commit();
        }

        public async Task<UserToListDTO> UpdateUser(UserUpdateDTO userUpdateDTO, int userId)
        {
            User user = await _dataContext.Users.FirstOrDefaultAsync(m => m.UserId == userId);
            if (user != null)
            {
                user.Name = userUpdateDTO.Name;
                user.Surname = userUpdateDTO.Surname;
                user.Mail = userUpdateDTO.Mail;
                user.Phone = userUpdateDTO.Phone;
                await _dataContext.SaveChangesAsync();
            }
            return _mapper.Map<UserToListDTO>(user);
        }
    }
}
