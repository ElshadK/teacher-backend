﻿using AutoMapper;
using HtmlAgilityPack;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using teacher_backend.DAL;
using teacher_backend.DTOs;
using teacher_backend.Entities;
using teacher_backend.Services.IServices;

namespace teacher_backend.Services
{
    public class HelperService : IHelperService
    {
        private readonly IMapper _mapper;
        private readonly DataContext _dataContext;
        public HelperService(DataContext dataContext, IMapper mapper)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }

        public async Task<List<SelectListDTO>> GetRegions()
        {
            List<Region> regions = await _dataContext.Regions.OrderBy(x => x.Name).ToListAsync();
            return _mapper.Map<List<SelectListDTO>>(regions);
        }

        public async Task<List<SelectListDTO>> GetSchools(int regionId)
        {
            List<School> schools = await _dataContext.Schools.Where(x=>x.RegionId==regionId).OrderBy(x => x.Name).ToListAsync();
            return _mapper.Map<List<SelectListDTO>>(schools);
        }

        public async Task<List<SelectListDTO>> GetSubjects()
        {
            List<Subject> subjects = await _dataContext.Subjects.OrderBy(x => x.Name).ToListAsync();
            return _mapper.Map<List<SelectListDTO>>(subjects);
        }    
    }
}
