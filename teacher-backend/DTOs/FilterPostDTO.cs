﻿namespace teacher_backend.DTOs
{
    public class FilterPostDTO
    {
        public int? UserId { get; set; }
        public int? FromSchoolId { get; set; }
        public int? ToSchoolId { get; set; }
        public int? FromRegionId { get; set; }
        public int? ToRegionId { get; set; }
        public int? SubjectId { get; set; }
        public int? LessonTimeMin { get; set; }
        public int? LessonTimeMax { get; set; }

    }
}
