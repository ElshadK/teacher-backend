﻿namespace teacher_backend.DTOs
{
    public class SchoolToListDTO
    {
        public int SchoolId { get; set; }
        public string Name { get; set; }
    }
}
