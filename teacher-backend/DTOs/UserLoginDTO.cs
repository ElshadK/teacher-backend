﻿namespace teacher_backend.DTOs
{
    public class UserLoginDTO
    {
        public string Mail { get; set; }
        public string Phone { get; set; }
        public string Password { get; set; }
    }
}
