﻿namespace teacher_backend.DTOs
{
    public class SubjectToListDTO
    {
        public int SubjectId { get; set; }
        public string Name { get; set; }
    }
}
