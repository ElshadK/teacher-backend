﻿using System.ComponentModel.DataAnnotations;

namespace teacher_backend.DTOs
{
    public class PostAddDTO
    {
        public int? FromSchoolId { get; set; }
        public int? ToSchoolId { get; set; }
        [Required]
        public int? FromRegionId { get; set; }
        [Required]
        public int? ToRegionId { get; set; }
        [Required]
        public int? SubjectId { get; set; }
        [Required]
        public int? LessonTime { get; set; }
        //public bool IsShowMail { get; set; }
        //public bool IsShowPhone { get; set; }
        public string Note { get; set; }
    }
}
