﻿namespace teacher_backend.DTOs
{
    public class SelectListDTO
    {
        public int Value { get; set; }
        public string Label { get; set; }
    }
}
