﻿namespace teacher_backend.DTOs
{
    public class PaginationDTO
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
}
