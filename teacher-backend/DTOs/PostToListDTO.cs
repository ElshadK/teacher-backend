﻿using System;
using teacher_backend.Entities;

namespace teacher_backend.DTOs
{
    public class PostToListDTO
    {
        public int PostId { get; set; }
        public int? LessonTime { get; set; }
        public bool IsShowMail { get; set; }
        public bool IsShowPhone { get; set; }
        public DateTime? CreatedAt { get; set; }
        public string Note { get; set; }

        public virtual RegionToListDTO FromRegion { get; set; }
        public virtual SchoolToListDTO FromSchool { get; set; }
        public virtual SubjectToListDTO Subject { get; set; }
        public virtual RegionToListDTO ToRegion { get; set; }
        public virtual SchoolToListDTO ToSchool { get; set; }
        public virtual UserToListDTO User { get; set; }
    }
}
