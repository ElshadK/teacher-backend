﻿using System.ComponentModel.DataAnnotations;

namespace teacher_backend.DTOs
{
    public class UserAddDTO
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Surname { get; set; }
        [Required]
        public string Mail { get; set; }
        public string Phone { get; set; }
        [Required]
        public string Password { get; set; }
    }
}
