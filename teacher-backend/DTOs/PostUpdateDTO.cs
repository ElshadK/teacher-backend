﻿namespace teacher_backend.DTOs
{
    public class PostUpdateDTO
    {
        public int? PostId { get; set; }
        public int? FromSchoolId { get; set; }
        public int? ToSchoolId { get; set; }
        public int? FromRegionId { get; set; }
        public int? ToRegionId { get; set; }
        public int? SubjectId { get; set; }
        public int? LessonTime { get; set; }
        //public bool IsShowMail { get; set; }
        //public bool IsShowPhone { get; set; }
        public string Note { get; set; }
    }
}
