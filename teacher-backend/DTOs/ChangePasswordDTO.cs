﻿namespace teacher_backend.DTOs
{
    public class ChangePasswordDTO
    {
        public string NewPassword { get; set; }
    }
}
