﻿namespace teacher_backend.DTOs
{
    public class UserToListDTO
    {
        public int UserId { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Mail { get; set; }
        public string Phone { get; set; }
    }
}
