﻿namespace teacher_backend.DTOs
{
    public class RegionToListDTO
    {
        public int RegionId { get; set; }
        public string Name { get; set; }
    }
}
