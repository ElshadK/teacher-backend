﻿using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using teacher_backend.Core;
using teacher_backend.Core.Utility;
using Microsoft.AspNetCore.Builder;

namespace teacher_backend.CustomMiddlewares
{
    public class ValidateTokenMiddleware
    {
        private readonly RequestDelegate _next;
        public ValidateTokenMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext httpContext, IUtilService utilService)
        {
            string aa = httpContext.Request.Path.Value.ToLower();
            if (!Constants.DenyTokenConfirmationPaths.Contains(httpContext.Request.Path.Value.ToLower()))
            {
                string tokenString = httpContext.Request.Headers[Constants.AuthorizationHeaderName].ToString();
                if (!utilService.IsValidToken(tokenString))
                {
                    httpContext.Response.StatusCode = StatusCodes.Status401Unauthorized;
                    return;
                }
            }
            await _next.Invoke(httpContext);
        }
    }
    public static class ValidateTokenMiddlewareExtensions
    {
        public static IApplicationBuilder UseMiddlewareClassTemplate(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ValidateTokenMiddleware>();
        }
    }
}
