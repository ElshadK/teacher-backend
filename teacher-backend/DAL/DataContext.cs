﻿using Microsoft.EntityFrameworkCore;
using teacher_backend.Entities;

namespace teacher_backend.DAL
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseLazyLoadingProxies();
        }


        //  Scaffold-DbContext "Server=DESKTOP-L6F31QE\SQLEXPRESS;Database=teacherdb;Trusted_Connection=false;User Id=sa;Password=El2017shad;" Microsoft.EntityFrameworkCore.SqlServer -OutputDir Entities -force
        //  Scaffold-DbContext "Server=RXITM155\SQLEXPRESS;Database=teacherdb;Trusted_Connection=false;User Id=sa;Password=El2017shad;" Microsoft.EntityFrameworkCore.SqlServer -OutputDir Entities -force

        public  DbSet<Post> Posts { get; set; }
        public  DbSet<Region> Regions { get; set; }
        public  DbSet<School> Schools { get; set; }
        public  DbSet<Subject> Subjects { get; set; }
        public  DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Post>().HasQueryFilter(m => !m.IsDeleted);
            modelBuilder.Entity<Region>().HasQueryFilter(m => !m.IsDeleted);
            modelBuilder.Entity<School>().HasQueryFilter(m => !m.IsDeleted);
            modelBuilder.Entity<Subject>().HasQueryFilter(m => !m.IsDeleted);
            modelBuilder.Entity<User>().HasQueryFilter(m => !m.IsDeleted);
        }
    }

}
