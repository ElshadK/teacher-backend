﻿using AutoMapper;
using teacher_backend.DTOs;
using teacher_backend.Entities;

namespace teacher_backend.Core
{
    public static class AutoMapperProfiles
    {
        public class AutoMapperProfile : Profile
        {
            public AutoMapperProfile()
            {
                CreateMap<Subject, SubjectToListDTO>();
                CreateMap<Region, RegionToListDTO>();
                CreateMap<School, SchoolToListDTO>();

                CreateMap<Subject, SelectListDTO>()
                    .ForMember(dest => dest.Label, opt => opt.MapFrom(src => src.Name.Trim()))
                    .ForMember(dest => dest.Value, opt => opt.MapFrom(src => src.SubjectId));
                CreateMap<Region, SelectListDTO>()
                    .ForMember(dest => dest.Label, opt => opt.MapFrom(src => src.Name.Trim()))
                    .ForMember(dest => dest.Value, opt => opt.MapFrom(src => src.RegionId));
                CreateMap<School, SelectListDTO>()
                    .ForMember(dest => dest.Label, opt => opt.MapFrom(src => src.Name.Trim()))
                    .ForMember(dest => dest.Value, opt => opt.MapFrom(src => src.SchoolId));

                CreateMap<Post, PostToListDTO>()
                     .ForMember(dest => dest.User, opt => opt.MapFrom(src => src.User));
                CreateMap<PostAddDTO, Post>();
                CreateMap<PostUpdateDTO, Post>();
                CreateMap<Post, PostUpdateDTO>();

                CreateMap<User, UserToListDTO>();
                CreateMap<UserAddDTO, User>();
            }
        }
    }
}
