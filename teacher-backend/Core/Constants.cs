﻿using System.Collections.Generic;

namespace teacher_backend.Core
{
    public class Constants
    {
        public const string AuthorizationHeaderName = "Authorization";

        public const string LoginPath = "/api/user/loginuser";
        public const string ResetPasswordPath = "/api/user/resetpassword";
        public const string RegistrationPath = "/api/user/adduser";

        public static readonly List<string> DenyTokenConfirmationPaths = new List<string>()
        {
            LoginPath, ResetPasswordPath, RegistrationPath
        };
    }
}
