﻿namespace teacher_backend.Core
{
    public class Messages
    {
        public const string InvalidModel = "Məlumatlar düzgün daxil edilməyib";
        public const string GeneralError = "Xəta baş verdi";
        public const string ValidationError = "Invalid model";
        public const string UserIsExist = "İstifadəçi mövcuddur";
        public const string PermissionDenied = "Əməliyyatın icrası üçün icazəniz yoxdur";
        public const string Success = "Uğurla icra edildi";
        public const string UsernameIsExist = "Bu email ilə qeydiyyat mövcuddur";
        public const string UserDoesNotExist = "İstifadəçi mövcud deyil";
        public const string InvalidUserCredentials = "İstifadəçi məlumatları yanlışdır";
        public const string DefaultBaseMapIsExist = "Default altlıq xəritə mövcuddur";
        public static string ResetPasswordConfirmationCodeMessage = "Kadastr portalı sistemində şifrənizin bərpası üçün təsdiq kodu (12 saat ərzində):";
        public static string InvalidDateToResetPasswordMessage = $"Təsdiq kodu doğru deyil.";
        public const string MailSended = "Elektron poçt ünvanınıza email göndərildi";
        public const string DataIsExistInBasket = "Məlumat səbətdə mövcuddur";
        public const string DataIsExistInFavourite = "Məlumat mövcuddur";
        public const string EmailDoesNotExist = "Qeyd edilmiş elektron poçt ünvanı ilə qeydiyyat edilməyib";
        public const string PasswordResetted = "Şifrəniz yeniləndi";
        public const string UserIsNotLogin = "Sistemə daxil olmamısınız";
        public const string BasketDidNotSendByCurrentUser = "Səbətdəki məhsullar alınmayıb";
        public const string InvalidFotoFormat = "Əlavə etdiyiniz şəkilin formatı uyğun deyil";
    }
}
