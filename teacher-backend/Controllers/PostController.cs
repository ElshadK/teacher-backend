﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using teacher_backend.Core;
using teacher_backend.Core.Utility;
using teacher_backend.DTOs;
using teacher_backend.Services.IServices;

namespace teacher_backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostController : ControllerBase
    {
        private readonly IPostService _postService;
        private readonly IUtilService _utilService;

        public PostController(IPostService postService, IUtilService utilService)
        {
            _postService = postService;
            _utilService = utilService;
        }


        [HttpPost("addPost")]
        public async Task<IActionResult> AddPost([FromBody] PostAddDTO postAddDTO)
        {
            try
            {
                string token = HttpContext.Request.Headers[Constants.AuthorizationHeaderName].ToString();

                if (!_utilService.IsValidToken(token)) return Unauthorized();

                int userId = _utilService.GetUserIdFromToken(token);

                return Ok(await _postService.AddPost(postAddDTO, userId));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("updatePost")]
        public async Task<IActionResult> UpdatePost([FromBody] PostUpdateDTO postUpdateDTO)
        {
            try
            {
                string token = HttpContext.Request.Headers[Constants.AuthorizationHeaderName].ToString();

                if (!_utilService.IsValidToken(token)) return Unauthorized();

                int userId = _utilService.GetUserIdFromToken(token);

                await _postService.UpdatePost(postUpdateDTO, userId);

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("getPosts")]
        public async Task<IActionResult> GetPosts([FromBody] FilterPostDTO filter)
        {
            try
            {
                PaginationDTO pagination = new PaginationDTO()
                {
                    PageNumber =1, //Convert.ToInt32(HttpContext.Request.Headers["PageNumber"]),
                    PageSize = 10 //Convert.ToInt32(HttpContext.Request.Headers["PageSize"])
                };

                return Ok(await _postService.GetPosts(filter, pagination));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("getPost/{postId}")]
        public async Task<IActionResult> GetPost(int postId)
        {
            try
            {
                string token = HttpContext.Request.Headers[Constants.AuthorizationHeaderName].ToString();

                if (!_utilService.IsValidToken(token)) return Unauthorized();

                int userId = _utilService.GetUserIdFromToken(token);

                return Ok(await _postService.GetPost(userId, postId));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("deletePost/{postId}")]
        public async Task<IActionResult> DeletePost(int postId)
        {
            try
            {
                string token = HttpContext.Request.Headers[Constants.AuthorizationHeaderName].ToString();

                if (!_utilService.IsValidToken(token)) return Unauthorized();

                int userId = _utilService.GetUserIdFromToken(token);

                await _postService.DeletePost(userId, postId);

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("getPostsForUser")]
        public async Task<IActionResult> GetPostsForUser()
        {
            try
            {
                string token = HttpContext.Request.Headers[Constants.AuthorizationHeaderName].ToString();

                int userId = _utilService.GetUserIdFromToken(token);

                PaginationDTO paginationDTO = new PaginationDTO()
                {
                    PageNumber = Convert.ToInt32(HttpContext.Request.Headers["PageNumber"]),
                    PageSize = Convert.ToInt32(HttpContext.Request.Headers["PageSize"])
                };

                return Ok(await _postService.GetPostsForUser(userId, paginationDTO));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
