﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using teacher_backend.Services.IServices;

namespace teacher_backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HelperController : ControllerBase
    {
        private readonly IHelperService _helperService;

        public HelperController(IHelperService helperService)
        {
            _helperService = helperService;
        }

        [HttpGet("getSubjects")]
        public async Task<IActionResult> GetSubjects()
        {
            try
            {
                return Ok(await _helperService.GetSubjects());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("getRegions")]
        public async Task<IActionResult> GetRegions()
        {
            try
            {
                return Ok(await _helperService.GetRegions());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("getSchools/{regionId}")]
        public async Task<IActionResult> GetSchools(int regionId)
        {
            try
            {
                return Ok(await _helperService.GetSchools(regionId));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
