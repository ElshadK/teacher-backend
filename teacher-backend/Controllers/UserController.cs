﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using teacher_backend.Core;
using teacher_backend.Core.Utility;
using teacher_backend.DTOs;
using teacher_backend.Services.IServices;

namespace teacher_backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IConfiguration _configuration;
        private readonly IUtilService _utilService;

        public UserController(IUserService userService, IConfiguration configuration, IUtilService utilService)
        {
            _userService = userService;
            _configuration = configuration;
            _utilService = utilService;
        }

        [HttpPost("addUser")]
        public async Task<IActionResult> AddUser([FromBody] UserAddDTO userAddDTO)
        {
            try
            {
                if (!ModelState.IsValid) return BadRequest(Messages.InvalidModel);

                if (await _userService.IsUserExist(userAddDTO.Mail, userAddDTO.Phone)) return BadRequest(Messages.UserIsExist);

                return Ok(await _userService.AddUser(userAddDTO));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("updateUser")]
        public async Task<IActionResult> UpdateUser([FromBody] UserUpdateDTO userUpdateDTO)
        {
            try
            {
                if (!ModelState.IsValid) return BadRequest(Messages.InvalidModel);

                string token = HttpContext.Request.Headers[Constants.AuthorizationHeaderName].ToString();

                if (!_utilService.IsValidToken(token)) return Unauthorized();

                int userId = _utilService.GetUserIdFromToken(token);

                return Ok(await _userService.UpdateUser(userUpdateDTO, userId));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("getUser")]
        public async Task<IActionResult> GetUser()
        {
            try
            {
                string token = HttpContext.Request.Headers[Constants.AuthorizationHeaderName].ToString();

                if (!_utilService.IsValidToken(token)) return Unauthorized();

                int userId = _utilService.GetUserIdFromToken(token);

                return Ok(await _userService.GetUser(userId));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("loginUser")]
        public async Task<IActionResult> LoginUser([FromBody] UserLoginDTO userLoginDTO)
        {
            try
            {
                string userSalt = await _userService.GetUserSalt(userLoginDTO.Mail);
                if (string.IsNullOrEmpty(userSalt)) return BadRequest(Messages.UserDoesNotExist);

                userLoginDTO.Password = SecurityHelper.HashPassword(userLoginDTO.Password, userSalt);
                UserToListDTO userDTO = await _userService.LoginUser(userLoginDTO);

                if (userDTO == null) return BadRequest(Messages.InvalidUserCredentials);

                DateTime expirationDate = DateTime.Now.AddHours(3);

                var claims = new List<Claim>();
                claims.Add(new Claim(ClaimTypes.NameIdentifier, userDTO.UserId.ToString()));
                claims.Add(new Claim(ClaimTypes.Email, userDTO.Mail.ToString()));
                claims.Add(new Claim(ClaimTypes.Expiration, expirationDate.ToString()));

                var key = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(_configuration.GetSection("JWTSettings:SecretKey").Value));
                var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(claims),
                    Expires = expirationDate,
                    SigningCredentials = creds
                };
                var tokenHandler = new JwtSecurityTokenHandler();
                var token = tokenHandler.CreateToken(tokenDescriptor);
                string tokenValue = tokenHandler.WriteToken(token);

                LoginResponseDTO loginResponseDTO = new LoginResponseDTO()
                {
                    Token = tokenValue,
                    User = userDTO
                };

                return Ok(loginResponseDTO);

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("changePassword")]
        public async Task<IActionResult> ChangePassword([FromBody] ChangePasswordDTO changePasswordDTO)
        {
            try
            {
                string token = HttpContext.Request.Headers[Constants.AuthorizationHeaderName].ToString();

                if (!_utilService.IsValidToken(token)) return Unauthorized();

                int userId = _utilService.GetUserIdFromToken(token);

                string userSalt = await _userService.GetUserSaltById(userId);

                if (string.IsNullOrEmpty(userSalt)) return BadRequest(Messages.UserDoesNotExist);

                changePasswordDTO.NewPassword = SecurityHelper.HashPassword(changePasswordDTO.NewPassword, userSalt);

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
