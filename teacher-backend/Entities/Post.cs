﻿using System;
using System.Collections.Generic;

#nullable disable

namespace teacher_backend.Entities
{
    public partial class Post
    {
        public int PostId { get; set; }
        public int? FromSchoolId { get; set; }
        public int? ToSchoolId { get; set; }
        public int? FromRegionId { get; set; }
        public int? ToRegionId { get; set; }
        public int? SubjectId { get; set; }
        public int? LessonTime { get; set; }
        public int UserId { get; set; }
        public bool? IsShowMail { get; set; }
        public bool? IsShowPhone { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? CreatedAt { get; set; }
        public string Note { get; set; }

        public virtual Region FromRegion { get; set; }
        public virtual School FromSchool { get; set; }
        public virtual Subject Subject { get; set; }
        public virtual Region ToRegion { get; set; }
        public virtual School ToSchool { get; set; }
        public virtual User User { get; set; }
    }
}
