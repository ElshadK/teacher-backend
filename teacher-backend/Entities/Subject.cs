﻿using System;
using System.Collections.Generic;

#nullable disable

namespace teacher_backend.Entities
{
    public partial class Subject
    {
        public Subject()
        {
            Posts = new HashSet<Post>();
        }

        public int SubjectId { get; set; }
        public string Name { get; set; }
        public bool IsDeleted { get; set; }

        public virtual ICollection<Post> Posts { get; set; }
    }
}
