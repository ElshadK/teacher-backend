﻿using System;
using System.Collections.Generic;

#nullable disable

namespace teacher_backend.Entities
{
    public partial class Region
    {
        public Region()
        {
            //PostFromRegions = new HashSet<Post>();
            //PostToRegions = new HashSet<Post>();
            //Schools = new HashSet<School>();
        }

        public int RegionId { get; set; }
        public int? Code { get; set; }
        public string Name { get; set; }
        public bool IsDeleted { get; set; }

        //public virtual ICollection<Post> PostFromRegions { get; set; }
        //public virtual ICollection<Post> PostToRegions { get; set; }
        //public virtual ICollection<School> Schools { get; set; }
    }
}
