﻿using System;
using System.Collections.Generic;

#nullable disable

namespace teacher_backend.Entities
{
    public partial class School
    {
        public School()
        {
            //PostFromSchools = new HashSet<Post>();
            //PostToSchools = new HashSet<Post>();
        }

        public int SchoolId { get; set; }
        public int? Code { get; set; }
        public int RegionId { get; set; }
        public string Name { get; set; }
        public bool IsDeleted { get; set; }

        //public virtual Region Region { get; set; }
        //public virtual ICollection<Post> PostFromSchools { get; set; }
        //public virtual ICollection<Post> PostToSchools { get; set; }
    }
}
