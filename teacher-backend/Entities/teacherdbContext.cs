﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace teacher_backend.Entities
{
    public partial class teacherdbContext : DbContext
    {
        public teacherdbContext()
        {
        }

        public teacherdbContext(DbContextOptions<teacherdbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Post> Posts { get; set; }
        public virtual DbSet<Region> Regions { get; set; }
        public virtual DbSet<School> Schools { get; set; }
        public virtual DbSet<Subject> Subjects { get; set; }
        public virtual DbSet<User> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Server=RXITM155\\SQLEXPRESS;Database=teacherdb;Trusted_Connection=false;User Id=sa;Password=El2017shad;");
            }
        }

        //Scaffold-DbContext "Server=RXITM155\SQLEXPRESS;Database=teacherdb;Trusted_Connection=false;User Id=sa;Password=El2017shad;" Microsoft.EntityFrameworkCore.SqlServer -OutputDir Entities -force

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Cyrillic_General_CI_AS");

            modelBuilder.Entity<Post>(entity =>
            {
                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.IsShowMail)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.IsShowPhone)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                //entity.HasOne(d => d.FromRegion)
                //    .WithMany(p => p.PostFromRegions)
                //    .HasForeignKey(d => d.FromRegionId)
                //    .HasConstraintName("FK_Posts_Regions");

                //entity.HasOne(d => d.FromSchool)
                //    .WithMany(p => p.PostFromSchools)
                //    .HasForeignKey(d => d.FromSchoolId)
                //    .HasConstraintName("FK_Posts_Schools");

                entity.HasOne(d => d.Subject)
                    .WithMany(p => p.Posts)
                    .HasForeignKey(d => d.SubjectId)
                    .HasConstraintName("FK_Posts_Subjects");

                //entity.HasOne(d => d.ToRegion)
                //    .WithMany(p => p.PostToRegions)
                //    .HasForeignKey(d => d.ToRegionId)
                //    .HasConstraintName("FK_Posts_Regions1");

                //entity.HasOne(d => d.ToSchool)
                //    .WithMany(p => p.PostToSchools)
                //    .HasForeignKey(d => d.ToSchoolId)
                //    .HasConstraintName("FK_Posts_Schools1");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Posts)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Posts_Users");
            });

            modelBuilder.Entity<Region>(entity =>
            {
                entity.Property(e => e.Name).HasMaxLength(50);
            });

            modelBuilder.Entity<School>(entity =>
            {
                entity.Property(e => e.Name).HasMaxLength(200);

                //entity.HasOne(d => d.Region)
                //    .WithMany(p => p.Schools)
                //    .HasForeignKey(d => d.RegionId)
                //    .OnDelete(DeleteBehavior.ClientSetNull)
                //    .HasConstraintName("FK_Schools_Regions");
            });

            modelBuilder.Entity<Subject>(entity =>
            {
                entity.Property(e => e.Name).HasMaxLength(50);
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.Mail).HasMaxLength(50);

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.Property(e => e.Phone).HasMaxLength(50);

                entity.Property(e => e.Surname).HasMaxLength(50);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
